<?php
/**
 * @file
 * Houses functions related to administrative tasks and the Admin UI.
 */

/**
 * Override of the Aggregator's administrative overview screen. Provides pagination of results
 * and checkboxes to bulk-delete Aggregator feeds, which adds to scalability.
 */
function aggregator_advanced_admin_overview() {
  $page = 0;
  if (isset($_GET['page'])) {
    $page = $_GET['page'];
  }
  $items_per_page = 50;
  $offset = $page * $items_per_page;
  
  $search = '';
  if (isset($_GET['search'])) {
    $search = $_GET['search'];
  }
  
  $query = db_select('aggregator_feed', 'f');
  // Add the search term, if the user is currently searching.
  if (!empty($search)) {
    $query->where('LOWER(f.title) LIKE :search', array(':search' => '%' . strtolower($search) . 
      '%'));
  }
  
  // Get the total number of feeds for use in creating the pager.
  $query->addExpression('COUNT(*)', 'total');
  $total = $query->execute()->fetchField();
 
  $query->leftJoin('aggregator_item', 'i', 'f.fid = i.fid');
  $query->groupBy('f.fid');
  /*
  $query->groupBy('f.title');
  $query->groupBy('f.url');
  $query->groupBy('f.refresh');
  $query->groupBy('f.checked');
  $query->groupBy('f.link');
  $query->groupBy('f.description');
  $query->groupBy('f.hash');
  $query->groupBy('f.etag');
  $query->groupBy('f.modified');
  $query->groupBy('f.image');
  $query->groupBy('f.block');
  */
  
  $query->fields('f', array('fid', 'title', 'url', 'refresh', 'checked', 'link', 'description', 
    'hash', 'etag', 'modified', 'image', 'block'));
  $query->addExpression('COUNT(i.iid)', 'items');
  $query->orderBy('f.title', 'ASC');
  $query->range($offset, $items_per_page);
  $result = $query->execute();
  
  $output = '<h3>' . t('Feed overview') . '</h3>';
  
  $output .= $total . ' ' . t('total feeds.') . '<br />';
  
  $output .= drupal_render(drupal_get_form('aggregator_advanced_admin_overview_search_form', 
    $search));
  
  $output .= drupal_render(drupal_get_form('aggregator_advanced_admin_overview_form', $result));
  
  
  // Add a pager.
  global $pager_page_array;
  global $pager_total;
  $pager_page_array = explode(',', $page);
  $pager_total = array(ceil($total / $items_per_page));
  $output .= theme('pager', array());

  $query = db_select('aggregator_category', 'c');
  $query->leftJoin('aggregator_category_item', 'ci', 'c.cid = ci.cid');
  $query->fields('c', array('cid', 'title'));
  $query->addExpression('COUNT(ci.iid)', 'items');
  $query->groupBy('c.cid');
  $query->groupBy('c.title');
  $query->orderBy('c.title', 'ASC');
  $result = $query->execute();
 
  $output .= '<h3>' . t('Category overview') . '</h3>';

  $header = array(t('Title'), t('Items'), t('Operations'));
  $rows = array();
  foreach ($result as $category) {
    $rows[] = array(l($category->title, "aggregator/categories/$category->cid"), 
      format_plural($category->items, '1 item', '@count items'), 
      l(t('edit'), "admin/config/services/aggregator/edit/category/$category->cid"));
  }
  $output .= theme('table', array(
    'header' => $header, 
    'rows' => $rows, 
    'empty' => t('No categories available. <a href="@link">Add category</a>.', 
      array('@link' => url('admin/config/services/aggregator/add/category')))
    )
  );
  
  // Add a link to bulk-check all feed URLs and log results.
  $output .= '<h3>' . t('Check all feed URLs and log results') . '</h3>';
  $output .= l(t('check URLs'), 'aggregator_advanced/check_feeds');
  $output .= '<p>' . t('This will use Drupal\'s Batch API to ping all feed URLs on the site and log
    any abnormal HTTP status codes returned. You can then view the results (if you have Views
    installed) and bulk-update redirected URLs, if desired.') . '</p>';
  
  // If a bulk check of feed URLs has already been done and log entries exist, provide a link to the
  // form that allows administrators to bulk-update redirected feed URLs (or delete log entries
  // for them).
  $logs_exist = db_select('aggregator_advanced_url_log', 'l')->fields('l')->range(0, 1)
    ->execute()->fetchField();
  if ($logs_exist) {
    $output .= '<h3>' . t('Feed URL log results') . '</h3>';
    $output .= l(t('redirected URLs - bulk operations'), 
      'admin/config/services/aggregator/redirected');
    $output .= '<p>' . t('Presents a form via which you can see which feeds were found to have
      redirected URLs (300-level HTTP status codes). You can choose to bulk-update these feeds\'
      URLs to the new (redirected) URLs or remove their log entries and ignore them. Be careful,
      since the redirected URL returned by the server may not always be complete or accurate.') .
      '</p>';
    // Let the user know about the normal, Views-integrated log report as well.
    $output .= '<p>' . t('Additionally, if you have the Views module enabled, a view is provided
      that will allow you to see all items in the log, which consists of every feed URL that 
      returned a non-200-level HTTP status code. This view is, by default, called "Aggregator feed 
      log" and available at !link or through the Reports page at !reports_link.', 
      array('!link' => l('admin/reports/aggregator_feed_log', 'admin/reports/aggregator_feed_log'),
      '!reports_link' => l('admin/reports', 'admin/reports')));
  }

  if (module_exists('feeds') && db_table_exists('feeds_source') && db_table_exists('feeds_importer')
    && user_access('administer feeds')) {
    $output .= '<h3>' . t('Convert Feeds module feeds to Aggregator feeds') . '</h3>';
    $output .= l(t('Convert Feeds module data'), 'aggregator_advanced/convert_feeds_data');
    $output .= '<p>' . t('You currently have the Feeds module installed and may have RSS feeds
      stored by that module. This will batch-convert them into Aggregator module feeds, so you
      can have everything in one place and uninstall Feeds.') . '</p>';
  }
  
  return $output;
}

/**
 * Presents the main table of Aggregator feeds as a form, rather than a simple table. This allows
 * us to insert delete checkboxes into each row, allowing for bulk-deletions of feeds. This makes
 * life easier for those pulling in large numbers of RSS feeds.
 */
function aggregator_advanced_admin_overview_form($form, &$form_state, $data) {
  $form = array(
    'rows' => array(
      '#tree' => TRUE,
    ),
    'actions' => array(
      '#type' => 'actions',
      'submit' => array(
        '#type' => 'submit',
        '#value' => t('Delete selected'),
      ),
    ),
    '#theme' => array('aggregator_advanced_admin_overview_form'),
  );

  foreach ($data as $feed) {
    $row = array();
    $row['fid'] = array(
      '#type' => 'hidden',
      '#value' => $feed->fid,
    );
    $row['title-hidden'] = array(
      '#type' => 'hidden',
      '#value' => $feed->title,
    );
    $row['title'] = array(
      '#type' => 'markup',
      '#markup' => l($feed->title, "aggregator/sources/$feed->fid")
    );
    $row['items'] = array(
      '#type' => 'markup',
      '#markup' => format_plural($feed->items, '1 item', '@count items')
    );
    $row['last_update'] = array(
      '#type' => 'markup',
      '#markup' => ($feed->checked ? 
        t('@time ago', array('@time' => format_interval(REQUEST_TIME - $feed->checked))) : 
        t('never'))
    );
    $row['next_update'] = array(
      '#type' => 'markup',
      '#markup' => ($feed->checked && $feed->refresh ? 
        t('%time left', 
          array('%time' => format_interval($feed->checked + $feed->refresh - REQUEST_TIME))) : 
        t('never'))
    );
    $row['edit'] = array(
      '#type' => 'markup',
      '#markup' => l(t('edit'), "admin/config/services/aggregator/edit/feed/$feed->fid")
    );
    $row['remove_items'] = array(
      '#type' => 'markup',
      '#markup' => l(t('remove items'), "admin/config/services/aggregator/remove/$feed->fid")
    );
    $row['update_items'] = array(
      '#type' => 'markup',
      '#markup' => l(t('update items'), "admin/config/services/aggregator/update/$feed->fid")
    );
    $row['delete'] = array(
      '#type' => 'checkbox',
    );
    $form['rows'][] = $row;
  }
 
  return $form;
  
}

/**
 * Confirmation form for bulk-deleting Aggregator feeds.
 */
function aggregator_advanced_admin_overview_confirm_form
  (&$form, &$form_state, $selected_fids = '') {
  
  $selected_fids = explode('&', $selected_fids);
  
  // Retrieve titles for all selected feed IDs.
  $result = db_select('aggregator_feed', 'f')
    ->fields('f', array('fid', 'title'))
    ->condition('fid', $selected_fids, 'IN')
    ->execute();
  
  $feeds = array();
  foreach ($result as $feed) {
    $feeds[$feed->fid] = t($feed->title);
  }
  
  $form = array();
  $form['selected'] = array(
    '#tree' => TRUE,
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
  );
  foreach ($feeds as $fid => $title) {
    $form['selected'][$fid] = array(
      '#type' => 'hidden',
      '#value' => $fid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) . '</li>' . "\n",
    );
  }
  
  $form['#submit'] = array(
    'aggregator_advanced_admin_overview_confirm_form_submit'
  );
  
  $confirm_question = format_plural(count($selected),
    'Are you sure you want to delete this feed?',
    'Are you sure you want to delete these feeds?'
  );
  
  $confirm_form = confirm_form($form, $confirm_question, 'admin/config/services/aggregator', 
    t('This action cannot be undone.'), t('Delete'), t('Cancel'));
  
  return $confirm_form;
}

/**
 * Submit handler for delete confirmation form. Actually deletes the feeds.
 */
function aggregator_advanced_admin_overview_confirm_form_submit(&$form, &$form_state) {
  $fids = $form_state['values']['selected'];
  foreach ($fids as $fid) {
    aggregator_advanced_delete_feed($fid);
  }
  drupal_set_message(t('The selected feeds were deleted.'));
  drupal_goto('admin/config/services/aggregator');
}

/**
 * Submit handler for bulk-delete form on Aggregator admin overview page.
 */
function aggregator_advanced_admin_overview_form_submit(&$form, &$form_state) {
  // Get all selected items.
  $selected = array();
  foreach ($form_state['values']['rows'] as $row) {
    if ($row['delete']) {
      $selected[$row['fid']] = $row['title-hidden'];
    }
  }
  $selected_string = implode('&', array_keys($selected));
  drupal_goto('admin/config/services/aggregator/confirm_delete/' . $selected_string);
}

/**
 * Drupal form defintion function for a simple search form that allows users to find specific
 * Aggregator feeds by title. This helps make the core Aggregator module scalable for sites that
 * need to store large numbers of feeds.
 */
function aggregator_advanced_admin_overview_search_form($form, &$form_state, $search = '') {
  $form = array(
    'search' => array(
      '#type' => 'textfield',
      '#title' => t('Search'),
      '#description' => t('To search for a feed with a given title, enter your keyword(s) here.'),
      '#default_value' => $search,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Go'),
      '#submit' => array('aggregator_advanced_admin_overview_search_form_submit'),
    ),
  );
  return $form;
}

/**
 * Submit handler for the search form that allows users to find specific Aggregator feeds by title.
 */
function aggregator_advanced_admin_overview_search_form_submit(&$form, &$form_state) {
  $search = check_plain($form_state['values']['search']);
  if (!empty($search)) {
    drupal_goto('admin/config/services/aggregator', array(
      'query' => array('search' => $search),
    ));
  }
}

/**
 * Creates a form via which users can view feeds whose URLs returned "redirect" (300-level) status
 * codes. This form allows users to bulk-update URLs or bulk-delete the feeds.
 */
function aggregator_advanced_redirected_url_form() {
  $form = array();
  $form['explanation'] = array(
    '#type' => 'markup',
    '#markup' => t('This page shows all RSS feed URLs that were found to redirect to new locations
      (and returned 300-level HTTP status codes) in the last check. You should use this form
      to review these URLs. You can then either update these feed URLs (and replace their
      current/old URL with the new/redirected one in the database) or delete the log entries
      for these (if you\'ve decided that they\'re okay).'),
  );
  $form['urls'] = array(
    '#tree' => TRUE,
  );
  
  $query = db_select('aggregator_advanced_url_log', 'l')  
    ->fields('l');
  $query->leftJoin('aggregator_feed', 'f', 'l.feed_id = f.fid');
  $query->fields('f', array('title'));
  $query->condition('l.status_code', 400, '<');
  $query->condition('l.status_code', 300, '>=');
  $result = $query->execute();
  
  foreach ($result as $obj) {
    $row = array();
    $row['checkbox'] = array(
      '#type' => 'checkbox',
    );
    $row['fid'] = array(
      '#type' => 'hidden',
      '#value' => $obj->feed_id,
    );
    $row['title'] = array(
      '#type' => 'markup',
      '#markup' => l(t($obj->title), '/aggregator/sources/' . $obj->feed_id),
    );
    $row['edit'] = array(
      '#type' => 'markup',
      '#markup' => l(t('edit'), 'admin/config/services/aggregator/edit/feed/' . $obj->feed_id, 
        array('query' => array('destination' => 'admin/config/services/aggregator/redirected'))),
    );
    $row['url'] = array(
      '#type' => 'markup',
      '#markup' => l($obj->url, $obj->url),
    );
    $row['redirect_url'] = array(
      '#type' => 'markup',
      '#markup' => l($obj->redirect_url, $obj->redirect_url),
    );
    $form['urls'][] = $row;
  }
  $form['operation'] = array(
    '#title' => t('Operation'),
    '#type' => 'select',
    '#options' => array(
      'update' => t('Permanently Update URLs to new locations'),
      'delete' => t('Delete log entries (and ignore redirected status)'),
    ),
    '#description' => t('Choose what you would like to do to these items when the form is 
      submitted.'),

  );
  $form['actions'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('aggregator_advanced_redirected_url_form_submit'),
  );
  $form['#theme'] = array('aggregator_advanced_redirected_url_form');
  return $form;
}

function aggregator_advanced_redirected_url_form_submit(&$form, &$form_state) {
  $op = $form_state['values']['operation'];
  // Get all selected feed ids.
  $selected_feeds = array();
  foreach ($form_state['values']['urls'] as $row) {
    if ($row['checkbox'] == 1) {
      $fid = $row['fid'];
      $selected_feeds[] = $fid;
    }
  }
  drupal_goto('admin/config/services/aggregator/log/confirm/' . $op . '/' . 
    implode('&', $selected_feeds));
}

function aggregator_advanced_redirected_url_confirm_form(&$form, &$form_state, $op, $fids) {
  // Make sure we have the needed arguments. If not, bail.
  if (!isset($op) || empty($op) || ($op != 'update' && $op != 'delete')) {
    drupal_goto('admin/config/services/aggregator/redirected');
  }
  $fids = explode('&', $fids);
  // Retrieve titles for all selected feed IDs.
  $result = db_select('aggregator_feed', 'f')
    ->fields('f', array('fid', 'title'))
    ->condition('fid', $fids, 'IN')
    ->execute();
  
  $feeds = array();
  foreach ($result as $feed) {
    $feeds[$feed->fid] = t($feed->title);
  }
  
  $form = array();
  $form['operation'] = array(
    '#type' => 'hidden',
    '#value' => $op,
  );
  $form['selected'] = array(
    '#tree' => TRUE,
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
  );
  foreach ($feeds as $fid => $title) {
    $form['selected'][$fid] = array(
      '#type' => 'hidden',
      '#value' => $fid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) . '</li>' . "\n",
    );
  }
  
  $form['#submit'] = array(
    'aggregator_advanced_redirected_url_confirm_form_submit'
  );
  
  if ($op == 'update') {
    $confirm_question = format_plural(count($fids),
      'Are you sure you want to update this feed\'s URL?',
      'Are you sure you want to update these feeds\' URLs?'
    );
    $button_text = t('Update');
  }
  elseif ($op == 'delete') {
    $confirm_question = format_plural(count($fids),
      'Are you sure you want to delete this feed\'s log entry?',
      'Are you sure you want to delete these feeds\' log entries?'
    );
    $button_text = t('Remove Log');
  }
  
  $confirm_form = confirm_form($form, $confirm_question, 
    'admin/config/services/aggregator/redirected', t('This action cannot be undone.'), 
    $button_text, t('Cancel'));
  
  return $confirm_form;
}

/**
 * Submit handler for confirmation form resulting from users choosing to update redirected feed URLs
 * or delete log entries.
 */
function aggregator_advanced_redirected_url_confirm_form_submit(&$form, &$form_state) {
  $op = $form_state['values']['operation'];
  $fids = $form_state['values']['selected'];
  if ($op == 'update') {
    // The user is updating each chosen feed's URL to the stored redirection URL.
    // Retrieve all new (redirected) URLs for chosen feeds.
    $redirected_urls = array();
    $result = db_select('aggregator_advanced_url_log', 'l')
      ->fields('l', array('feed_id', 'redirect_url'))
      ->condition('feed_id', $fids, 'IN')
      ->execute();
    foreach ($result as $obj) {
      $redirected_urls[$obj->feed_id] = $obj->redirect_url;
    }
    foreach ($fids as $fid) {
      // Update the URL in the database.
      $new_url = $redirected_urls[$fid];
      db_update('aggregator_feed')
        ->fields(array('url' => $new_url))
        ->condition('fid', $fid)
        ->execute();
      // Now, remove the old log entry, since the URL has changed.
      db_delete('aggregator_advanced_url_log')
        ->condition('feed_id', $fid)
        ->execute();
    }
    drupal_set_message(t('The selected feed URLs have been updated.'));
  }
  elseif ($op == 'delete') {
    // The user is removing each chosen feed's log entry.
    foreach ($fids as $fid) {
      db_delete('aggregator_advanced_url_log')
        ->condition('feed_id', $fid)
        ->execute();
    }
    drupal_set_message(t('The selected feed log entries have been removed.'));
  }
  drupal_goto('admin/config/services/aggregator/redirected');
}
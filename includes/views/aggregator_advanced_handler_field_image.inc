<?php
/**
 * @file
 * Displays the image retrieved from an RSS feed (and prevents XSS attacks).
 */

class aggregator_advanced_handler_field_image extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);
    return filter_xss($value, array('img'));
  }
}

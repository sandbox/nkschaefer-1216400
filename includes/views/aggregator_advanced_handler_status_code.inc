<?php
/** 
 * @file
 * Views handler defintion for user-readable status code text.
 *
 * This handler transforms a numerical code - either a negative number returned by 
 * drupal_http_request() or the Advanced Aggregator parser or an HTTP status code - into human-
 * readable text.
 */

class aggregator_advanced_handler_status_code extends views_handler_field {
  
  function construct() {
    parent::construct();
    $this->additional_fields['status_code'] = 'status_code';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $code = $values->{$this->aliases['status_code']};
    
    $text = '';
    
    // All status code definitions taken from http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
    // Other (negative) pseudo-status codes used by the Feeds module are defined in
    // drupal_http_request: 
    // http://api.drupal.org/api/drupal/includes--common.inc/function/drupal_http_request/7
    
    switch ($code) {
      case -1001:
        $text = t('Unable to parse URL');
        break;
      case -1002:
        $text = t('Missing Schema');
        break;
      case -1003:
        $text = t('Invalid Schema');
        break;
      case -99:
        // This is a custom error code -- if the administrator has enabled the parser
        // provided by this module (Advanced parser), then this code is logged when
        // we get a valid HTTP status code, but there is a problem parsing the feed XML.
        $text = t('Error parsing feed XML');
        break;
      // It's not totally clear what a return value of 0 means (it's not the documentation
      // of drupal_http_request(), but it appears to mean that the URL was not valid
      // and no response was able to come back.
      case 0:
        $text = t('Invalid URL');
        break;
      case 100:
        $text = t('Continue');
        break;
      case 101:
        $text = t('Switching Protocols');
        break;
      case 200:
        $text = t('OK');
        break;
      case 201:
        $text = t('Created');
        break;
      case 202:
        $text = t('Accepted');
        break;
      case 203:
        $text = t('Non-Authoritative Information');
        break;
      case 204:
        $text = t('No Content');
        break;
      case 205:
        $text = t('Reset Content');
        break;
      case 206:
        $text = t('Partial Content');
        break;
      case 300:
        $text = t('Multiple Choices');
        break;
      case 301:
        $text = t('Moved Permanently');
        break;
      case 302:
        $text = t('Found');
        break;
      case 303:
        $text = t('See Other');
        break;
      case 304:
        $text = t('Not Modified');
        break;
      case 305:
        $text = t('Use Proxy');
        break;
      case 306:
        $text = t('Switch Proxy');
        break;
      case 307:
        $text = t('Temporary Redirect');
        break;
      case 400:
        $text = t('Bad Request');
        break;
      case 401:
        $text = t('Unauthorized');
        break;
      case 402:
        $text = t('Payment Required');
        break;
      case 403:
        $text = t('Forbidden');
        break;
      case 404:
        $text = t('Not Found');
        break;
      case 405:
        $text = t('Method Not Allowed');
        break;
      case 406:
        $text = t('Not Acceptable');
        break;
      case 407:
        $text = t('Proxy Authentication Required');
        break;
      case 408:
        $text = t('Request Timeout');
        break;
      case 409:
        $text = t('Conflict');
        break;
      case 410:
        $text = t('Gone');
        break;
      case 411:
        $text = t('Length Required');
        break;
      case 412:
        $text = t('Precondition Failed');
        break;
      case 413:
        $text = t('Request Entity Too Large');
        break;
      case 414:
        $text = t('Request-URI Too Long');
        break;
      case 415:
        $text = t('Unsupported Media Type');
        break;
      case 416:
        $text = t('Requested Range Not Satisfiable');
        break;
      case 417:
        $text = t('Expectation Failed');
        break;
      case 500:
        $text = t('Internal Server Error');
        break;
      case 501:
        $text = t('Not Implemented');
        break;
      case 502:
        $text = t('Bad Gateway');
        break;
      case 503:
        $text = t('Service Unavailable');
        break;
      case 504:
        $text = t('Gateway Timeout');
        break;
      case 505:
        $text = t('HTTP Version Not Supported');
        break;
    }
    return $text;
  }
}
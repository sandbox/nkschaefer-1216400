<?php
/**
 * @file
 * Views Field handler to present a link to delete an entry from the Aggregator feed URL log.
 */
 
class aggregator_advanced_handler_feed_edit_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['fid'] = 'fid';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }
  
   function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);
  }
  
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $value = $this->get_value($values, 'fid');
    return $this->render_link($this->sanitize_value($value), $values);
  }
  
  function render_link($data, $values) {
    // Bail out if the user doesn't have the right permission.
    if (!user_access('administer news feeds')) {
      return;
    }
    
    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "admin/config/services/aggregator/edit/feed/$data";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    return $text;
  }
}

<?php
/**
 * @file
 * Houses theme functions.
 */

/**
 * Themes a URL for an Aggregator feed on the "display" users will see.
 */
function theme_aggregator_feed_source_url(&$vars) {
  $url = $vars['url'];
  return '<strong>' . t('URL') . ': </strong>' . l($url, $url);
}

/**
 * Themes a "Last checked" date for an Aggregator feed on the "display" users will see.
 */
function theme_aggregator_feed_last_checked(&$vars) {
  $updated = $vars['updated'];
  return '<strong>' . t('Updated') . ': </strong>' . $updated;
}

/**
 * Renders the "Admin Overview" form for Aggregator feeds.
 */
function theme_aggregator_advanced_admin_overview_form($variables) {
  $form = $variables['form'];
  $output = '';
  // Note: the Title column has to hold two hidden fields, in addition to the visible title.
  $header = array(array('data' => t('Title'), 'colspan' => 3), t('Items'), t('Last update'), 
    t('Next update'), array('data' => t('Operations'), 'colspan' => '3'), t('Delete'));
  $rows = array();
  foreach (element_children($form['rows']) as $row_key) {
    $row = &$form['rows'][$row_key];
    $table_row = array();
    foreach (element_children($row) as $cell) {
      $table_row[] = drupal_render(&$row[$cell]);
    }
    $rows[] = $table_row;
  
  }
  
  $output .= theme('table', 
    array(
      'header' => $header, 
      'rows' => $rows, 
      'empty' => t('No feeds available. <a href="@link">Add feed</a>.', 
        array('@link' => url('admin/config/services/aggregator/add/feed')))
    )
  );
  
  // Add the submit button and hidden form magic stuff.
  $output .= drupal_render_children($form);
  
  return $output;
}

/**
 * Renders a form to allow administrators to view redirected URLs (for feeds that returned a 
 * 300-level HTTP status code) and update them to the new (redirected) ones, if desired.
 */
function theme_aggregator_advanced_redirected_url_form($variables) {
  $form = $variables['form'];
  $output = '';
  $output .= drupal_render($form['explanation']);
  $output .= '<br /><br />';
  
  $header = array(
    t('Select'), 
    array('data' => t('Title'), 'colspan' => 2), 
    t('Edit'), 
    t('URL'), 
    t('New URL')
  );
  
  $rows = array();
  foreach (element_children($form['urls']) as $row_key) {
    $row = &$form['urls'][$row_key];
    $table_row = array();
    foreach (element_children($row) as $cell) {
      $table_row[] = drupal_render(&$row[$cell]);
    }
    $rows[] = $table_row;
  }
  
  $output .= theme('table', 
    array(
      'header' => $header, 
      'rows' => $rows, 
      'empty' => t('There are currently no logged instances of a feed URL returning a "redirect" 
        status code.')
    )
  );

  // Add the "operations," submit button, and Drupal form magic.
  $output .= drupal_render_children($form);
  return $output;
}
<?php
/**
 * @file
 * Contains functions related to bulk operations - large tasks that use Drupal's Batch API to 
 * act on many things at once.
 */

/**
 * Uses Batch API to cycle through all existing feeds on the site and log bad (non-200-level) HTTP 
 * status codes. These will then show up in a View provided by this module, and a form will
 * be provided to allow users to bulk-update redirected (300-level) feed URLs, if desired.
 */
function aggregator_advanced_check_all_feeds_main() {
  $operations = array();
  $num_to_check = 25;
  $operations[] = array('aggregator_advanced_check_all_feeds_aux', array($num_to_check));
  $batch = array(
    'operations' => $operations,
    'title' => t('Checking URLs of all RSS feeds'),
    'progress_message' => t('checking URLs...'),
    'finished' => 'aggregator_advanced_check_all_feeds_complete',
    'file' => drupal_get_path('module', 'aggregator_advanced') . 
      '/includes/aggregator_advanced.bulk.inc',
  );
  batch_set($batch);
  batch_process('admin/config/services/aggregator');
}

/**
 * Auxillary helper function for main batch API callback (above). 
 *
 * @param $num_to_check 
 *  the number of RSS feed URLs to ping on each function call
 * @param &$context 
 *  an array of information maintained by Drupal across multiple function calls
 */
function aggregator_advanced_check_all_feeds_aux($num_to_check, &$context) {
  if (!isset($context['sandbox']['total_num'])) {
    $query = db_select('aggregator_feed', 'f');
    $query->addExpression('COUNT(*)', 'total');
    $total_num = $query->execute()->fetchField();
    $context['sandbox']['total_num'] = $total_num;
    $context['sandbox']['offset'] = 0;
    // Prepare buckets in the $context array that will store information to be sent to the
    // "finished" callback, where it will be shown to the user.
    $context['results']['status_codes'] = array();
    $context['results']['status_codes']['ok'] = 0;
    $context['results']['status_codes']['redirect'] = 0;
    $context['results']['status_codes']['other'] = 0;
  }
  $offset = $context['sandbox']['offset'];
  
  // Get the right number of URLs and associated feed IDs from the database.
  $query = db_select('aggregator_feed', 'f')
    ->fields('f', array('fid', 'url'));
  $query->range($offset, $num_to_check);
  $result = $query->execute();
  
  while ($obj = $result->fetchObject()) {
    $response = aggregator_advanced_check_feed_url($obj->fid, $obj->url);
    if ($response == 1) {
      // The URL was determined to be valid.
      $context['results']['status_codes']['ok']++;
    }
    elseif ($response == 0) {
      // The URL was a redirect.
      $context['results']['status_codes']['redirect']++;
    }
    elseif ($response == -1) {
      // The URL was invalid.
      $context['results']['status_codes']['other']++;
    }
    $offset++;
    $progress = $offset / $context['sandbox']['total_num'];
    $context['finished'] = $progress;
  }
  //$offset += $num_to_check;
  $progress = $offset / $context['sandbox']['total_num'];
  if ($progress > 1) $progress = 1;
  $context['finished'] = $progress;
  $context['sandbox']['offset'] = $offset;
}

/**
 * The function that actually checks feed URLs and logs the results. 
 * An integer value is returned that provides some rough information about the results, so they can
 * be shown to the user in the "finished" Batch API callback. 
 *
 * @param $fid 
 *  the Aggregator feed ID
 * @param $url 
 *  the URL of the aggregator feed
 *
 * @return 
 *  an integer specifying, roughly, what the outcome of the URL check was.
 *   1 signifies "OK" (200-level status code)
 *   0 signifies "redirect" (300-level status code)
 *   -1 signifies "invalid" (anything other than 200 or 300-level). This could be anything from
 *     a 404 error to an invalid URL (i.e. missing schema) to an internal server error, and
 *     beyond.
 */
function aggregator_advanced_check_feed_url($fid, $url) {
  // Define integer value that will be returned
  $ret_val = 1;
  
  // Check the URL
  $response = drupal_http_request($url, array(
    'header' => array('User-Agent' => 'User-Agent: Drupal (+http://drupal.org/)'), 
    'method' => 'GET', 
    'timeout' => 3
    ));
  
  // Handle redirects
  // If a URL was redirected, we'll store the URL it redirected to in the database and let
  // the administrator check it before updating and saving the feed.
  if (isset($response->redirect_code) && isset($response->redirect_url) && 
    !empty($response->redirect_url)) {
    
    $redirect_url = $response->redirect_url;
    $exists = db_select('aggregator_advanced_url_log', 'l')
      ->fields('l')
      ->condition('feed_id', $fid)
      ->range(0, 1)
      ->execute()
      ->fetchObject();
    // A log entry exists; update it.
    if ($exists) {
      $fail_count = $exists->fail_count;
      $fail_count++;
      $fields = array(
        'url' => $url,
        'redirect_url' => $redirect_url,
        'status_code' => $response->redirect_code,
        'fail_count' => $fail_count,
        'last_checked' => REQUEST_TIME,
      );
      db_update('aggregator_advanced_url_log')
        ->fields($fields)
        ->condition('feed_id', $fid)
        ->execute();
    }
    // A log entry does not exist; insert a new one.
    else {
       $fields = array(
         'feed_id' => $fid,
         'url' => $url,
         'redirect_url' => $redirect_url,
         'status_code' => $response->redirect_code,
         'fail_count' => 1,
         'last_checked' => REQUEST_TIME,
       );
      db_insert('aggregator_advanced_url_log')
        ->fields($fields)
        ->execute();
    }
    $ret_val = 0;
  }
  
  // Handle other bad status codes
  elseif (!in_array($response->code, array(200, 201, 202, 203, 204, 205, 206))) {
    // Log the bad status code and unpublish the feed.
    $exists = db_select('aggregator_advanced_url_log', 'l')
      ->fields('l')
      ->condition('feed_id', $fid)
      ->range(0, 1)
      ->execute()
      ->fetchObject();
    // A log entry exists; update it.
    if ($exists) {
      $fail_count = $exists->fail_count;
      $fail_count++;
      $fields = array(
        'status_code' => $response->code,
        'fail_count' => $fail_count,
        'last_checked' => REQUEST_TIME,
      );
      db_update('aggregator_advanced_url_log')
        ->fields($fields)
        ->condition('feed_id', $fid)
        ->execute();
    }
    // A log entry does not exist; create a new one.
    else {
      $fields = array(
        'feed_id' => $fid,
        'url' => $url,
        'status_code' => $response->code,
        'fail_count' => 1,
        'last_checked' => REQUEST_TIME
      );
      db_insert('aggregator_advanced_url_log')
        ->fields($fields)
        ->execute();
    }
    $ret_val = -1;
  }
  else {
    // Otherwise, we encountered a "good" status code. Here, we'll just check to see if a log
    // entry exists for the URL (if it encountered a "bad" status code on a previous check).
    // If so, we'll delete the log entry.
    $exists = db_select('aggregator_advanced_url_log', 'l')
      ->fields('l')
      ->condition('feed_id', $fid)
      ->range(0, 1)
      ->execute()
      ->fetchObject();
    if ($exists) {
      db_delete('aggregator_advanced_url_log')
        ->condition('feed_id', $fid)
        ->execute();
    }
  }
  return $ret_val;
}

/**
 * "Complete" callback function for cleaning up when the bulk operation is complete.
 */
function aggregator_advanced_check_all_feeds_complete($success, $results, $operations) {
  $msg = t('All RSS feed URLs have been checked.');
  $msg .= '<br />';
  $count_ok = $results['status_codes']['ok'];
  $count_redirect = $results['status_codes']['redirect'];
  $count_other = $results['status_codes']['other'];
  $total = $count_ok + $count_redirect + $count_other;
  $msg .= t('There were @total feeds', array('@total' => $total));
  $msg .= '<br />';
  $msg .= t('@count_ok URLs were valid', array('@count_ok' => $count_ok));
  $msg .= '<br />';
  $msg .= t('@count_redirect URLs redirected to new URLs.', 
    array('@count_redirect' => $count_redirect));
  $msg .= '<br />';
  $msg .= t('@count_other URLs returned unexpected status codes and have been logged.', 
    array('@count_other' => $count_other));
  $msg .= '<br /><br />';
  $msg .= t('Scroll down to the bottom of the page for links to a log report and a form where you
    may bulk-update URLs of feeds that have been redirected.');
  drupal_set_message($msg);
}

/**
 * Main callback for running the bulk conversion of Feeds module feed nodes into Aggregator feeds.
 * Uses Batch API to perform the operation.
 */
function aggregator_advanced_convert_feed_nodes_main() {
  // Make sure Feeds module data exists before attempting to retrieve it.
  if (!module_exists('feeds') || !db_table_exists('feeds_source') || 
    !db_table_exists('feeds_importer')) {
    
    drupal_goto('admin/config/services/aggregator');
  }
  
  // First, we'll check all Field API fields associated with all node types used as feeds by the
  // Feeds module. Then, for those not yet attached, we'll attach them to Aggregator feeds so they
  // can be populated when doing the bulk conversion.
  $feed_fields = field_info_instances('aggregator_feed', 'aggregator_feed');
  $result = db_select('feeds_importer', 'fi')
    ->fields('fi')
    ->execute();
  foreach ($result as $item) {
    $config = unserialize($item->config);
    if (isset($config['content_type']) && !empty($config['content_type'])) {
      $node_type = $config['content_type'];
      // Get all fields associated with this content type.
      $node_fields = field_info_instances('node', $node_type);
      foreach ($node_fields as $field_name => $field_info) {
        if (!isset($feed_fields[$field_name])) {
          // Check to see if we're allowed to attach the field to the Aggregator feed
          // entity type.
          $field = field_read_field($field_name);
          if (!empty($field['entity_types']) && 
            !in_array('aggregator_feed', $field['entity_types'])) {
            continue;
          }
          unset($field_info['id']);
          unset($field_info['field_id']);
          $field_info['entity_type'] = 'aggregator_feed';
          $field_info['bundle'] = 'aggregator_feed';
          field_create_instance($field_info);
          // Refresh the $feed_fields array to reflect all fields currently attached.
          $feed_fields = field_info_instances('aggregator_feed', 'aggregator_feed');
        }
      }
    }
  }
  
  $operations = array();
  $num_to_check = 25;
  $operations[] = array('aggregator_advanced_convert_feed_nodes_aux', array($num_to_check));
  $batch = array(
    'operations' => $operations,
    'title' => t('Converting Feed nodes from Feeds module into Aggregator module feeds'),
    'finished' => 'aggregator_advanced_convert_feed_nodes_complete',
    'file' => drupal_get_path('module', 'aggregator_advanced') . 
      '/includes/aggregator_advanced.bulk.inc',
  );
  batch_set($batch);
  batch_process('admin/config/services/aggregator');
}

function aggregator_advanced_convert_feed_nodes_aux($num_to_check, &$context) {
  if (!isset($context['sandbox']['total_num'])) {
    $query = db_select('feeds_source', 'f');
    $query->addExpression('COUNT(*)', 'total');
    // Make sure items aren't already saved in aggregator table.
    $query->leftJoin('aggregator_feed', 'af', 'f.source = af.url');
    $query->where('af.url IS NULL');
    $total_num = $query->execute()->fetchField();
    $context['sandbox']['total_num'] = $total_num;
    $context['sandbox']['offset'] = 0;
    
    // As we go through each feed importer type, we'll create an Aggregator category for each one.
    // Since Feeds inconveniently keeps its data stored in serialized arrays, we'll just keep 
    // information about which categories we've already saved in the &$context array.
    $context['sandbox']['categories'] = array();
    
    // Get an array of all fields currently associated with Aggregator feeds (should include all 
    // fields associated with Feed module nodes):
    $context['sandbox']['feed_fields'] = field_info_instances('aggregator_feed', 'aggregator_feed');
    
    // And this will store all Field API data associated with each node type we encounter.
    // This prevents us from needing to hit the database each time we examine a node.
    $context['sandbox']['node_fields'] = array();
    
    // Prepare buckets in the $context array that will store information to be sent to the
    // "finished" callback, where it will be shown to the user.
    $context['results']['categories'] = array();
    $context['results']['count'] = 0;
    $context['results']['deleted_nodes'] = 0;
  }
  $offset = $context['sandbox']['offset'];
  $saved_categories = $context['sandbox']['categories'];
  $feed_fields = $context['sandbox']['feed_fields'];
  $node_fields = $context['sandbox']['node_fields'];
  
  // Get the right number of URLs and associated feed IDs from the database.
  $query = db_select('feeds_source', 'fs')
    ->fields('fs', array('id', 'feed_nid', 'source'));
  // Get information about the type of feed, so we can create an Aggregator category if necessary.
  $query->leftJoin('feeds_importer', 'fi', 'fs.id = fi.id');
  $query->fields('fi', array('config'));
  // Get the node title, if it exists.
  $query->leftJoin('node', 'n', 'fs.feed_nid = n.nid');
  $query->fields('n', array('title', 'type'));
  // Make sure the feed is not already saved in the Aggregator table.
  $query->leftJoin('aggregator_feed', 'af', 'fs.source = af.url');
  $query->where('af.url IS NULL');
  $query->range($offset, $num_to_check);
  $result = $query->execute();
  
  while ($obj = $result->fetchObject()) {
  
    // If the item is linked to a feed node, that node must exist.
    if ($obj->feed_nid > 0 && (!isset($obj->title) || empty($obj->title))) {
      continue;
    }
    
    // Here is what we have to do here:
    
    // Insert a row into the aggregator_feed table representing the feed that the Feeds module
    // feed formerly represented.
    
    // If the Feeds module feed was a node, delete the node.
    
    // Check what type of feed it was (types of feeds are called "Importers" by the Feeds module).
    // Store each type as an Aggregator category; keep track of already-saved categories via the
    // $saved_categories array, which will persist across function calls.
    
    // Associate the feed with its category.
  
    if (isset($obj->source) && !empty($obj->source)) {
      
      $config = unserialize($obj->config);
      
      // Here, we either have a valid node title, or it's a standalone feed importer whose
      // title can be taken from the Feeds Importer config array.
      $title = '';
      if (isset($obj->title) && !empty($obj->title)) {
        $title = $obj->title;
      }
      elseif (isset($config->name) && !empty($config->name)) {
        $title = $config->name;
      }
      
      // Note: many of these fields (i.e. link, description, image, hash, etag) will be populated
      // when feed data is imported.
      $insert = array(
        'title' => $title,
        'url' => $obj->source,
        'refresh' => isset($config['expire_period']) ? $config['expire_period'] : 3600,
        'checked' => 0,
        'queued' => 0,
        'link' => '',
        'description' => '',
        'image' => '',
        'hash' => '',
        'etag' => '',
        'modified' => 0,
        'block' => 0,  // Assuming there are a lot of feeds, enabling blocks for them 
                // could make the admin/structure/block page time out.
      );
      // The insert query should automatically return the database ID. We need it in order
      // to associate the feed with a category.
      $fid = db_insert('aggregator_feed')->fields($insert)->execute();
      $context['results']['count']++;
    
      if ($obj->feed_nid > 0) {
        // Get all Field API data associated with the node.
        if (isset($obj->type) && !empty($obj->type)) {
          if (!isset($node_fields[$obj->type])) {
            $node_fields[$obj->type] = field_info_instances('node', $obj->type);
          }
          // Go through each field associated with the node that's also associated with
          // Aggregator feeds.
          
          // Create fake objects; only load the actual node and feed if there are fields
          // that need to be saved.
          $node = new stdClass();
          $feed = new stdClass();
          $has_match = FALSE;
          foreach ($node_fields[$obj->type] as $field_name => $field_info) {
            if (isset($feed_fields[$field_name])) {
              if (!$has_match) {
                $node = node_load($obj->feed_nid);
                $feed = aggregator_feed_load($fid);
                $has_match = TRUE;
              }
              $field_items = $node->{$field_name};
              $feed->{$field_name} = $field_items;
            }
          }
          if ($has_match) {
            // Save field data for the feed.
            //field_attach_presave('aggregator_feed', &$feed);
            field_attach_insert('aggregator_feed', $feed);
          }
        }
        node_delete($obj->feed_nid);
        $context['results']['deleted_nodes']++;
      }
      
      if (!isset($saved_categories[$obj->id])) {
        // Create a new Aggregator category for this feed importer.
        $cat_name = $config['name'];
        $cat_desc = $config['description'];
        $cid = db_select('aggregator_category', 'ac')
          ->fields('ac', array('cid'))
          ->condition('title', $cat_name)
          ->execute()->fetchField();
        if (!$cid) {
          // Insert queries return database insert IDs when possible.
          $cid = db_insert('aggregator_category')
            ->fields(array('title' => $cat_name, 'description' => $cat_desc, 'block' => 5))
            ->execute();
        }
        $saved_categories[$obj->id] = $cid;
        $context['results']['categories'][] = $cat_name;
      }
      else {
        $cid = $saved_categories[$obj->id];
      }
      
      // Now associate the category with the feed.
      db_insert('aggregator_category_feed')->fields(array('fid' => $fid, 'cid' => $cid))->execute();
    
    }
  }
  
  $offset += $num_to_check;
  
  $progress = $offset / $context['sandbox']['total_num'];
  if ($progress > 1) $progress = 1;
  $context['finished'] = $progress;
  $context['sandbox']['offset'] = $offset;
  $context['sandbox']['categories'] = $saved_categories;
  $context['sandbox']['node_fields'] = $node_fields;
}

/**
 * "Finished" callback for batch operation to convert Feeds module feeds into Aggregator module
 * feeds.
 */
function aggregator_advanced_convert_feed_nodes_complete($success, $results, $operations) {
  $total = $results['count'];
  $deleted_nodes = $results['deleted_nodes'];
  $categories = $results['categories'];
  $msg = t('Conversion of Feeds module feeds to Aggregator feeds complete.');
  $msg .= '<br />';
  $msg .= t('@total new feeds have been created', array('@total' => $total));
  $msg .= '<br />';
  $msg .= t('@deleted_nodes Feeds module feed nodes have been deleted', 
    array('@deleted_nodes' => $deleted_nodes));
  $msg .= '<br />';
  if (count($categories) > 0) {
    $msg .= t('The following Aggregator feed categories have been created, based on Feeds module 
      data:');
    $msg .= '<br />';
    foreach ($categories as $name) {
      $msg .= t($name) . '<br />';
    }
  }
  $msg .= '<br /><br />';
  $msg .= t('You should now uninstall the Feeds module to clear up room in your database. If you
    stored imported feed items as nodes, you should also visit !link and delete all old
    feed items that are no longer needed.', 
      array('!link' => l('admin/content/node', 'admin/content/node')));
  drupal_set_message($msg);
}
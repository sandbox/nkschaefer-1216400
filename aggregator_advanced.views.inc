<?php
/**
 * @file
 * Provides Views integration for the Advanced Aggregator module.
 */

/**
 * Implements hook_views_data().
 *
 * Exposes data from the Feed URL log table to Views.
 */
function aggregator_advanced_views_data() {
  $data['aggregator_advanced_url_log'] = array(
    'table' => array(
      'group' => t('Feed URL Log'),
      'join' => array(
        'aggregator_feed' => array(
          'left_table' => 'aggregator_feed',
          'left_field' => 'fid',
          'field' => 'feed_id',
        ),
      ),
    ),
    // Feed URL
    'url' => array(
      'title' => t('URL'),
      'help' => t('The URL of a feed that returned an abnormal HTTP status code'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // HTTP Response Code
    'status_code' => array(
      'title' => t('Status Code'),
      'help' => t('The status code returned by HTTP requests to the feed URL'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
    ),
    // Human-readable translation of status code
    'status_code_text' => array(
      'field' => array(
        'title' => t('Status Code Meaning'),
        'help' => t('A human-readable translation of the HTTP status code'),
        'handler' => 'aggregator_advanced_handler_status_code',
      ),
    ),
    // Fail Count
    'fail_count' => array(
      'title' => t('Fail Count'),
      'help' => t('The number of times requests for the URL have failed'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
    ),
    // Last Checked
    'last_checked' => array(
      'title' => t('Last Checked'),
      'help' => t('The time importing was attempted from the URL'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
    ),
    // Provide a link to delete individual log entries.
    'delete_log' => array(
      'field' => array(
        'title' => t('Delete Link'),
        'help' => t('A simple link to delete an entry from the Aggregator feed URL log.'),
        'handler' => 'aggregator_advanced_handler_log_delete_link',
      ),
    ),
  );
  return $data;
}

/**
 * Implements hook_views_data_alter().
 *
 * Provides a Views relationship that allows for access of fields added to Aggregator items' parent
 * Aggregator feeds.
 */
function aggregator_advanced_views_data_alter(&$data) {  
  
  // Create a Views relationship that will allow users to link Aggregator Items to their parent
  // Aggregator Feeds. This allows us to access all Aggregator Feeds' fields when creating a 
  // View based on Aggregator Items.
  
  $data['aggregator_item']['fid'] = array(
    'title' => t('Parent feed'),
    'help' => t('The Aggregator feed that produced the child Aggregator item'),
    'relationship' => array(
      'base' => 'aggregator_feed',
      'base field' => 'fid',
      'label' => 'parent_feed',
      'field' => 'fid',
    ),
  );
  
  // Also expose the aggregator_feed table as a base table. This allows the log table provided 
  // by this module to have Views integration, since it's linked to the aggregator_feed table.
  $data['aggregator_feed']['table']['base'] = array(
    'field' => 'fid',
    'title' => t('Aggregator feed'),
    'help' => t('Aggregator feeds are parent RSS feeds that can import child Aggregator items,
      which are individual entries in a feed.'),
  );
  
  // And provide an "edit" link for Aggregator feeds that will show up in Views.
  $data['aggregator_feed']['edit_feed'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('A link to edit the Aggregator feed.'),
      'handler' => 'aggregator_advanced_handler_feed_edit_link',
    ),
  );
  
  // Expose the feed description; allow Aggregator module to prevent XSS attacks
  $data['aggregator_feed']['description'] = array(
  	'field' => array(
      'title' => t('Description'),
      'help' => t('The parent website\'s description; comes from the <description> element in the 
        feed.'),
      'handler' => 'views_handler_field_aggregator_xss',
  	),
  );
  
  // Expose the feed image to Views as well.
  $data['aggregator_feed']['image'] = array(
  	'field' => array(
  	  'title' => t('Image'),
  	  'help' => t('A logo image retrieved from an RSS feed channel, if available.'),
  	  'handler' => 'aggregator_advanced_handler_field_image',
  	),
  );
}
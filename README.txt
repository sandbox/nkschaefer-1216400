README

The Advanced Aggregator module is designed to make Drupal's core Aggregator module (in version
7 and beyond) more useful, flexible, and scalable. Changes made to the Aggregator module in version
7.x make it much more useful than it was in 6.x and before, and relying on the large and sometimes-
unwieldy Feeds module will become unnecessary for some users. Additionally, while the "feeds as
nodes" concept was originally very useful, the fact that we can now add fields to non-node entities,
and the fact that core Aggregator now has Views integration, makes exposing feeds and feed items
as nodes not only unnecessary, but impractical as well. The core aggregator module keeps imported
feed items in their own database table, which, for sites with a large number of feeds, is a huge
performance gain over dumping everything into the node table, which will likely be crowded with
many other types of content.

In the interest of adding flexibility and scalability, the Advanced Aggregator module does the
following:

1) Exposes core Aggregator feeds as fieldable entities. Users can now add fields to Aggregator
	feeds via a tab at the admin/config/services/aggregator page.
	
2) Adds a Views relationship that allows users to access properties of parent Aggregator feeds
	for views built on child Aggregator items. This means that each imported feed item can now
	access fields added to its parent feed.
	
3) For users with lots of feeds, makes the Aggregator feed administration page 
	(admin/config/services/aggregator) scalable by adding pagination and only showing 50 feeds at a 
	time. Additionally, checkboxes are provided that will allow users to delete multiple feeds at a 
	time, and a search box is provided to search through feeds by title.
	
4) A table is provided that will store broken and/or redirected Feed URLs. Users can choose to
	run a mass check of feed URLs on the site, using Batch API, which will ping the URL of each
	stored feed and store any problems in the log table. Users can view a report based on this
	table via a view provided by this module, and a form is also provided through which users can 
	bulk-update redirected URLs.
	
5) A utility is provided for users who (like myself) had formerly relied on the Feeds module and
	had lots of Feed nodes. The utility converts the feed nodes to core Aggregator feeds and attempts
	to preserve any Field API data associated (if you're upgrading from Drupal 6, be sure to 
	upgrade all CCK data to Field API first). Once you've done this, you can safely uninstall
	Feeds and drop its tables.

6) An alternative fetcher and parser are provided via the Aggregator module's hooks. The parser
	is designed to avoid a problem that can arise with importing feeds: title and author fields
	can contain too much data and result in database errors when insertion is attempted. The parser
	provided by this module (Advanced parser) lets the default parser act, then truncates the title
	and author fields, on word boundaries, to the maximum allowable size in the database.
	
	The fetcher is designed to log errors to the provided error log. The default fetcher simply
	calls drupal_set_message(), which is not useful for sites with lots of feeds importing on cron
	runs. This fetcher (Advanced fetcher) logs all bad (non-200-level) HTTP status codes to the error
	log, along with redirected URLs (if a 300-level status code is encountered). 
	
	Additionally, the parser sometimes encounters errors when trying to parse a document as RSS XML.
	If the parser encounters an error, it also stores the error in the log, where administrators
	can view it later.
	
=============
INSTALLATION
=============

Install the module normally, as you would any other module.

Install Views, if you'd like to see the log report.

If you'd like to convert Feeds module data to use Aggregator instead, first ensure all Fields
	are properly configured on your Feed nodes. After this, visit admin/config/services/aggregator
	and click the link (toward the bottom) to convert Feeds module data. Once complete, uninstall
	the Feeds module.

If you'd like to use the parser and fetcher provided by this module (recommended, as they should 
	onlyhelp, not hurt anything), go to admin/config/services/aggregator and click the "Settings" 
	tab. Select "Advanced parser" and "Advanced fetcher."

To configure field display and appearance, use the tabs on the admin/config/services/aggregator
	page - the UI should be the same as for other fieldable entities (like nodes).

To batch-check all feed URLs on the site, follow the link toward the bottom of the page at
	admin/config/services/aggregator.

Everything else should be provided at admin/config/services/aggregator, except the log report,
	which is at admin/reports/aggregator_feed_log (if you have Views enabled). To change the way
	the report looks, use the Views module UI.